(*
 * Copyright (c) 2013 MindTouch Inc
 *)

open Core.Std

type t

(*
val default_max_auto_redirects : int
val base_endpoint_score : int
val default_timeout : Time.t
 *)

(* TODO: 
 * GlobalCookies
 * logger
 *)

(* TODO: set a default timeout value *)
val of_string : string -> t
val to_string : t -> string

(*
val of_uri : Uri.t -> ?timeout:Time.t -> t
 *)

(* TODO:
 * AddEndpoint
 * RemoveEndPoint
 *)

(*
val to_uri : t -> Uri.t
val at : t -> string list -> t
 *)
(* TODO: AtPath *)
(* val with_query : t -> (string * string list) list -> t *)

val with_query': t -> query:(string * string) -> t
val with_query_quote_pipe: (string * string) -> t -> t
(*
val with_credentials : t -> string -> string -> t
val without_credentials : t -> t
 *)

(* TODO:
 * WithCookieJar
 * WithoutCookieJar
 * WithAutoRedirects
*)

(*
val with_header : t -> (string * string) -> t
val with_headers : t -> (string * string) list -> t
val without_header : t -> string -> t
val without_headers : t -> t
 *)

(* TODO
 * WithPreHandler
 * WithPostHandler
 * WithOutHandlers
 *)

(*
val with_timeout : t -> Time.t -> t
val with_trailing_slash : t -> t
val without_trailing_slash: t -> t
 *)

(* TODO
 * WithSegmentDoubleEncoding
 * WithoutSegmentDoubleEncoding
 *)

(* TODO: Implement all the HTTP verbs, sync and async versions *)
