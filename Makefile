# sandbox:
#	ocamlbuild -use-ocamlfind -pkg lwt,ocsigenserver.ext,ocsigenserver,ocsigenserver.http -tag thread  -cflags "-w A-4-33-41-42-43-34-44" -cflags -strict-sequence -cflags -principal sandbox.native

plug:
	ocamlbuild -use-ocamlfind -pkg uri,lwt,ocsigenserver.ext,ocsigenserver.http -tag thread -cflags "-w A-4-33-41-42-43-34-44" -cflags -strict-sequence -cflags -principal test_plug.native

# plug.native
clean:
	rm -rf _build *.native
