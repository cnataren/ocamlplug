open Lwt

(* a simple function to access the content of the response *)
let content = function
    | { Ocsigen_http_frame.frame_content = Some v } -> Ocsigen_stream.string_of_stream 8000 (Ocsigen_stream.get v)
    | _ -> return ""

(* launch both requests in parallel *)
let t = Lwt_list.map_p Ocsigen_http_client.get_url [ "http://ocsigen.org/"; "http://stackoverflow.com/" ]

(* maps the result through the content function *)
let t2 = t >>= Lwt_list.map_p content

(* launch the event loop *)
let result = Lwt_main.run t2
