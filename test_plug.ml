(* Client Plug code *)

let uu =
  Plug.of_string "http://google.com"
  |> Plug.with_query' ~query:("foo", "bar")
  |> Plug.to_string
  |> print_string

let u =
  Plug.of_string "http://mindtouch.com"
  |> Plug.with_query_quote_pipe ("foo", "bar")
  |> Plug.to_string
  |> print_string