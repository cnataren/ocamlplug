open Core.Std

type t = {
  hostname: string option;
  queries: (string * string list) list;
  timeout: Time.Span.t option
}

let of_string s = { hostname = Some s; queries = []; timeout = None; }

let parse_simple_query (query: string * string) =
  let (name, value) = query in
  (name, [value])

let with_query' t ~query =
  { t with queries = parse_simple_query query :: t.queries }

let with_query_quote_pipe query t =
  { t with queries = parse_simple_query query :: t.queries }

let to_string t = match t.hostname with
| Some hostname -> hostname
| None -> ""